# SharedStaticProject

This demonstrates sharing a static member variable between a DLL and an application

## Results

So apparently, DLL's do not shared the same instances of static variables. Acessing MyClass::my_value from the main application is different than from the DLL. This is really annoying.

Check out the output if you want proof.

## The solution
checkout the `working` branch.
