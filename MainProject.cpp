#include <stdio.h>
#include "MyClass.h"

int main(int argc, char* argv[])
{
  printf("in main program my_value has address %p\n", &MyClass::my_value);
	printf("in main program my_value initializes to %i\n", MyClass::my_value);
	MyClass::my_value = 3;
	printf("in main program my_value is now %i\n", MyClass::my_value);
  printf("calling into DLL...\n");

	MyClass my_class;
	my_class.load_dll();

	printf("in main program my_value is now %i\n", MyClass::my_value);
	return 0;
}

