#include "MyDll.h"
#include "MyClass.h"
#include <stdio.h>

extern "C" DLL_EXPORT MyDll *RegisterDll()
{
  return new MyDll();
}

void MyDll::function1()
{
	printf("hello from function1\n");
  printf("in DLL, my_value has address %p\n", &MyClass::my_value);
}

void MyDll::function2()
{
	printf("in DLL, my_value starts as %i\n", MyClass::my_value);
}

void MyDll::function3()
{
	MyClass::my_value = 5;
	printf("in DLL, my_value is now %i\n", MyClass::my_value);
}
