#include "MyDll.h"
#include "MyClass.h"
#include <string>
#include <stdio.h>

#ifdef _WIN32
#include "win_dlfcn.h"
#else
#include "dlfcn.h"
#endif

int MyClass::my_value = 0;

void MyClass::load_dll()
{
	//load dll here and call it's function
#ifdef _WIN32
	std::string fullname = "dynamic_lib.dll";
#else
	std::string fullname = "libdynamic_lib.so";
#endif

	void *dlHandle = dlopen(fullname.c_str(), RTLD_LAZY | RTLD_GLOBAL);
	if (!dlHandle)
	{
		printf("Failed to load plugin: %s\n", dlerror());
		return;
	}

  typedef union {
   MyDll *(*func)();
    void *ptr;
  } fptr_union_t;

	fptr_union_t func;
  func.ptr = dlsym(dlHandle, "RegisterDll");

	if (!func.ptr)
	{
		printf("Failed to resolve %s\n", dlerror());
		return;
	}

  MyDll *my_class = func.func();

  my_class->function1();
  my_class->function2();
  my_class->function3();
}
